import React, {Fragment} from 'react'
import PropType from 'prop-types'
import {connect} from 'react-redux'
import {onLogout as onLogoutAction} from 'store/actions/auth/auth'
import {DashboardNav, DashboardSideNav} from 'components/Dashboard/index'
import './DashboardLayout.css'

class DashboardLayout extends React.Component {
  state = null

  static propTypes = {
    children: PropType.element.isRequired,
  }

  render() {
    const {props} = this

    return (
      <Fragment>
        <DashboardNav currentUser={props.currentUser} onLogout={props.onLogout} />
        <div className="layout__body">
          <div className="layout__sidenav">
            <DashboardSideNav />
          </div>
          <div className="layout__content">{props.children}</div>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser.displayName,
  teamName: state.team.name,
})

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(onLogoutAction()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardLayout)
