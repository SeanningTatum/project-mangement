import React, {Fragment} from 'react'
import PropType from 'prop-types'
import NavigationBar from 'components/Navigation/NavigationBar/NavigationBar'

import './Layout.css'

const Layout = ({children, hideNavigation}) => (
  <Fragment>
    {!hideNavigation && <NavigationBar />}
    <div className="layout__body">{children}</div>
  </Fragment>
)

Layout.propTypes = {
  children: PropType.element.isRequired,
  hideNavigation: PropType.bool.isRequired,
}

export default Layout
