import React, {Component} from 'react'
import {FormGroup, Container, Col, Form} from 'reactstrap'
import Input from 'components/UI/Input/Input'
import Button from 'components/UI/Button/Button'
import {connect} from 'react-redux'
import {onLogin as onLoginAction} from 'store/actions/auth/auth'

export class Login extends Component {
  state = {
    form: {
      email: '',
      password: '',
    },
  }

  inputChangedHandler = (formName, event) => {
    const {state} = this
    const newForm = {...state.form}
    newForm[formName] = event.target.value

    this.setState({form: newForm})
  }

  render() {
    const {state, props} = this

    return (
      <Container>
        <Col xs={6} className="mx-auto mt-3">
          <Form onSubmit={e => props.onLogin(e, state.form)}>
            {/* Email */}
            <FormGroup>
              <label htmlFor="email">Email</label>
              <Input
                placeholder="johndoe@email.com"
                name="email"
                type="email"
                value={state.form.email}
                onChange={event => this.inputChangedHandler('email', event)}
              />
            </FormGroup>

            {/* Password */}
            <FormGroup>
              <label htmlFor="password">Password</label>
              <Input
                placeholder="Enter password here"
                name="password"
                type="password"
                value={state.form.password}
                onChange={event => this.inputChangedHandler('password', event)}
              />
            </FormGroup>
            <div className="text-right">
              <Button type="submit" color="primary">
                Login
              </Button>
            </div>
          </Form>
        </Col>
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onLogin: (e, form) => {
    e.preventDefault()
    return dispatch(onLoginAction(form))
  },
})

export default connect(
  null,
  mapDispatchToProps
)(Login)
