import React, {Component} from 'react'
import './CreateTeam.css'
// Components
import {FormGroup, Container, Col, Form} from 'reactstrap'
import Input from 'components/UI/Input/Input'
import Button from 'components/UI/Button/Button'
// GraphQL
import {Mutation} from 'react-apollo'
import {CREATE_TEAM_MUTATION} from 'utils/apollo'
// Redux
import {saveCurrentTeam as saveCurrentTeamAction} from 'store/actions/team/team'
import {connect} from 'react-redux'

class CreateTeam extends Component {
  state = {
    form: {
      teamName: '',
    },
  }

  /**
   * Description:
   * This function calls the createTeamHandler which
   * is a graphQL mutation which is passed in the render method.
   *
   * This function prepares the variables and modulurizes the code
   *
   * @param {object} event
   * @param {function} createTeamHandler
   */
  onCreateTeam = (event, createTeamHandler) => {
    event.preventDefault()

    const {state, props} = this

    createTeamHandler({
      variables: {
        teamName: state.form.teamName,
        userUID: props.userUID,
      },
    })
  }

  inputChangedHandler = (inputName, event) => {
    const {state} = this
    const updatedForm = {...state.form}
    updatedForm[inputName] = event.target.value
    this.setState({form: updatedForm})
  }

  render() {
    const {state, props} = this

    return (
      <Mutation mutation={CREATE_TEAM_MUTATION} onCompleted={data => props.saveCurrentTeam(data)}>
        {(createTeam, {loading, error}) => {
          if (loading) {
            return <h1>Creating Team...</h1>
          }

          if (error) {
            return <h1>An error has occurred</h1>
          }

          return (
            <Container className="container--create-team">
              <Col md={6} className="mx-auto">
                <div className="mb-3">
                  <h5 className="mb-1">Create a team.</h5>
                  <small>
                    <strong>Create a team to collaborate with</strong>
                  </small>
                </div>

                <Form onSubmit={event => this.onCreateTeam(event, createTeam)}>
                  <FormGroup>
                    <label htmlFor="teamName">Team Name</label>
                    <Input
                      placeholder="Your team name here"
                      name="teamName"
                      value={state.form.teamName}
                      onChange={event => this.inputChangedHandler('teamName', event)}
                    />
                  </FormGroup>
                  <div className="text-right">
                    <Button type="submit" color="primary">
                      Create Team
                    </Button>
                  </div>
                </Form>
              </Col>
            </Container>
          )
        }}
      </Mutation>
    )
  }
}

const mapStateToProps = state => ({
  userUID: state.auth.currentUser.uid,
})

const mapDispatchToProps = dispatch => ({
  saveCurrentTeam: data => {
    const teamData = data.createTeam

    return dispatch(saveCurrentTeamAction(teamData))
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTeam)
