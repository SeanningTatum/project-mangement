import React, {Component} from 'react'
import {Button} from 'components/UI/index'
import ProjectList from 'components/Projects/ProjectList/ProjectList'
import ProjectSummary from 'components/Projects/ProjectSummary/ProjectSummary'
import AddProjectModal from 'components/Projects/AddProjectModal/AddProjectModal'

import './Projects.css'

class Projects extends Component {
  state = {
    openModal: false,
  }

  toggleModal = () => this.setState(prevState => ({openModal: !prevState.openModal}))

  render() {
    const {state} = this

    return (
      <div className="container--projects">
        <div className="header--projects mt-5 mb-3">
          <h3>Projects</h3>
          <Button color="primary" onClick={this.toggleModal}>
            Add Project
          </Button>
        </div>

        <div className="mb-3">
          <ProjectSummary />
        </div>

        <div>
          <ProjectList />
        </div>

        <AddProjectModal open={state.openModal} toggle={this.toggleModal} />
      </div>
    )
  }
}

export default Projects
