import React, {Component} from 'react'
import {Query} from 'react-apollo'
import qs from 'query-string'
import axios from 'axios'
import {Container} from 'reactstrap'
import JoinTeamForm from 'components/JoinTeam/JoinTeamForm/JoinTeamForm'
import {JOIN_TEAM_QUERY} from 'utils/apollo'

import firebase from '../../utils/firebase'

import './JoinTeam.css'

class JoinTeam extends Component {
  state = {
    teamID: '',
    inviteKey: '',
    email: '',
    hasMounted: false,
    form: {
      nickname: '',
      password: '',
      confirmPassword: '',
    },
  }

  async componentDidMount() {
    const {props} = this
    const queryParams = qs.parse(props.location.search)

    // Check if the invite-key is valid, if not redirect user
    try {
      await this.checkInviteKey()

      this.setState({
        teamID: queryParams.teamID,
        email: queryParams.email,
        inviteKey: queryParams.inviteKey,
        hasMounted: true,
      })
    } catch (error) {
      props.history.push('/no-key')
    }
  }

  checkInviteKey = ({inviteKey, teamID, email}) => {
    const data = {inviteKey, teamID, email}
    return axios.post('http://localhost:8080/team/check-invite-key', data)
  }

  createUser = async (event, joinTeamMutation) => {
    event.preventDefault()
    const {state} = this

    try {
      const {user} = await firebase
        .auth()
        .createUserWithEmailAndPassword(state.email, state.form.password)

      await user.updateProfile({displayName: state.form.nickname, emailVerified: true})

      await joinTeamMutation({
        variables: {
          teamID: state.teamID,
          email: state.email,
          userUID: user.uid,
          displayName: state.form.nickname,
          inviteKey: state.inviteKey,
        },
      })

      user.sendEmailVerification()
    } catch (error) {
      console.error(error)
    }
  }

  // Delete the user in firebase if error occurs in graphQL server
  deleteUser = async () => {
    const user = await firebase.auth().currentUser
    if (user) user.delete()
  }

  inputChangedHandler = (event, formName) => {
    const {state} = this

    const tempForm = {...state.form}
    tempForm[formName] = event.target.value

    this.setState({form: tempForm})
  }

  render() {
    const {state} = this

    // Had to create a state.hasMounted because
    // the graphQL query would run even if there was
    // no state.teamID
    return (
      state.hasMounted && (
        <Query query={JOIN_TEAM_QUERY} variables={{teamID: state.teamID}}>
          {({data, loading, error}) => {
            if (loading) return <h1>Loading...</h1>

            if (error) return <h1>{error.message}</h1>

            return (
              <Container className="container--join-team">
                <div className="info-header mb-2">
                  <h5>You've been invited by {data.Team.name}</h5>
                  <small className="mb-3">Sign up to accept the invitation</small>
                </div>

                <div className="form--join-team">
                  <JoinTeamForm
                    inputChangedHandler={this.inputChangedHandler}
                    nickname={state.form.nickname}
                    password={state.form.password}
                    confirmPassword={state.form.confirmPassword}
                    onSubmitHandler={this.createUser}
                    onErrorHandler={this.deleteUser}
                  />
                </div>
              </Container>
            )
          }}
        </Query>
      )
    )
  }
}

export default JoinTeam
