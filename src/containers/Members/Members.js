import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {validateEmail} from 'utils/helperFunctions'
import {Query} from 'react-apollo'
import {FETCH_TEAM_MEMBERS_QUERY} from 'utils/apollo'
// Components
import Button from 'components/UI/Button/Button'
import MemberList from 'components/Members/MemberList/MemberList'
import InviteMembersModal from 'components/Members/InviteMembersModal/InviteMembersModal'
import {Container} from 'reactstrap'

import './Members.css'

class Members extends Component {
  static propTypes = {
    currentUser: PropTypes.shape({
      displayName: PropTypes.string,
      uid: PropTypes.string,
    }),
    teamID: PropTypes.string,
  }

  state = {
    modalOpen: false,
    inviteLength: [1],
    form: {
      email1: '',
    },
  }

  /**
   * Description:
   *
   * onSubmitHandler is passed to inviteMembersModal
   * the inviteMembersHandler is the graphQL handler method
   * passed back to this function so we can put input the
   * variables needed.
   *
   * @param {function} inviteMembersHandler
   */
  onSubmitHandler = async inviteMembersHandler => {
    const {state, props} = this

    // If email is valid and not blank, put it into an array
    const tempArray = Object.values(state.form).filter(email => {
      if (email.trim() !== '' && validateEmail(email.trim()) === true) return email
    })

    const retval = await inviteMembersHandler({
      variables: {
        teamID: props.teamID,
        emails: tempArray,
        sentBy: props.currentUser.displayName,
        senderEmail: props.currentUser.email,
      },
    })

    console.log(retval)
  }

  formChangeHandler = async (email, event) => {
    const {state} = this
    const tempForm = state.form

    tempForm[email] = event.target.value
    await this.setState({form: tempForm})
  }

  /**
   * Description:
   *
   * When this function is invoked add an element
   * to the state.inviteLength array
   */
  addInviteLength = () => {
    this.setState(prevState => ({
      inviteLength: [...prevState.inviteLength, prevState.inviteLength.length + 1],
    }))
  }

  toggleModal = () => this.setState(prevState => ({modalOpen: !prevState.modalOpen}))

  render() {
    const {state, props} = this

    return (
      <Query query={FETCH_TEAM_MEMBERS_QUERY} variables={{teamID: props.teamID}}>
        {({loading, error, data}) => {
          if (loading) return <h5>Loading...</h5>

          if (error) return <h5>An Error has occurred</h5>

          const {members} = data.Team

          return (
            <Container className="mt-5">
              <div className="header mb-3">
                <div>
                  <h5 className="d-inline mr-3">Team Members</h5>
                  <span>{members.length} total</span>
                </div>
                <Button color="primary" onClick={this.toggleModal}>
                  Invite members
                </Button>
              </div>

              <div>
                <MemberList members={members} />
              </div>

              <InviteMembersModal
                modalOpen={state.modalOpen}
                toggleModal={this.toggleModal}
                onSubmitHandler={this.onSubmitHandler}
                inviteLength={state.inviteLength}
                addInviteLength={this.addInviteLength}
                formChangeHandler={this.formChangeHandler}
                form={state.form}
              />
            </Container>
          )
        }}
      </Query>
    )
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  teamID: state.team.id,
})

export default connect(
  mapStateToProps,
  null
)(Members)
