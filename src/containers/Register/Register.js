import React, {Component} from 'react'
import {FormGroup, Container, Col, Form} from 'reactstrap'
import {connect} from 'react-redux'
import Input from 'components/UI/Input/Input'
import Button from 'components/UI/Button/Button'
import {onRegister as onRegisterAction} from 'store/actions/auth/auth'

export class Register extends Component {
  state = {
    form: {
      fullName: '',
      email: '',
      password: '',
      confirmPassword: '',
    },
  }

  inputChangedHandler = (formName, event) => {
    const {state} = this
    const newForm = {...state.form}
    newForm[formName] = event.target.value

    this.setState({form: newForm})
  }

  render() {
    const {state, props} = this

    return (
      <Container>
        <Col xs={6} className="mx-auto mt-3">
          <Form onSubmit={event => props.onRegister(event, state.form)}>
            {/* Email */}
            <FormGroup>
              <label htmlFor="email">Email</label>
              <Input
                placeholder="johndoe@email.com"
                name="email"
                type="email"
                value={state.form.email}
                onChange={event => this.inputChangedHandler('email', event)}
              />
            </FormGroup>

            {/* Full Name */}
            <FormGroup>
              <label htmlFor="fullName">Full Name</label>
              <Input
                placeholder="John Doe"
                name="fullName"
                value={state.form.fullName}
                onChange={event => this.inputChangedHandler('fullName', event)}
              />
            </FormGroup>

            {/* Password */}
            <FormGroup>
              <label htmlFor="email">Password</label>
              <Input
                placeholder="Enter your password"
                name="email"
                value={state.form.password}
                onChange={event => this.inputChangedHandler('password', event)}
                type="password"
              />
            </FormGroup>

            {/* Confirm Password */}
            <FormGroup>
              <label htmlFor="email">Confirm Password</label>
              <Input
                placeholder="Confirm your password"
                name="email"
                value={state.form.confirmPassword}
                onChange={event => this.inputChangedHandler('confirmPassword', event)}
                type="password"
              />
            </FormGroup>

            <div className="text-right">
              <Button color="primary" type="submit">
                Submit
              </Button>
            </div>
          </Form>
        </Col>
      </Container>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onRegister: (event, form) => {
    event.preventDefault()

    return dispatch(onRegisterAction(form))
  },
})

export default connect(
  null,
  mapDispatchToProps
)(Register)
