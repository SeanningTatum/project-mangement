/* eslint-disable import/order, no-underscore-dangle */

// React
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

import registerServiceWorker from './registerServiceWorker'
import './index.css'

// Router
import {BrowserRouter} from 'react-router-dom'

// Redux
import {createStore, applyMiddleware, compose, combineReducers} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'

// Reducers
import authReducer from 'store/reducers/auth/auth'
import teamReducer from 'store/reducers/team/team'

// Apollo
import {ApolloProvider} from 'react-apollo'
import apolloClient from 'utils/apollo'

const rootReducer = combineReducers({auth: authReducer, team: teamReducer})

const composeEnhancers =
  process.env.NODE_ENV === 'development'
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : null || compose

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))

const app = (
  <ApolloProvider client={apolloClient}>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </ApolloProvider>
)

ReactDOM.render(app, document.getElementById('root'))
registerServiceWorker()
