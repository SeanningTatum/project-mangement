/* eslint-disable import/order, react/no-did-update-set-state */
import React, {Component} from 'react'
import PropTypes from 'prop-types'

// Router
import {Route, Switch, Redirect, withRouter} from 'react-router-dom'

// Redux
import {connect} from 'react-redux'
import {getCurrentUser as getCurrentUserAction} from 'store/actions/auth/auth'
import {getCurrentTeam as getCurrentTeamAction} from 'store/actions/team/team'

// Containers
import Register from 'containers/Register/Register'
import Login from 'containers/Login/Login'
import CreateTeam from 'containers/CreateTeam/CreateTeam'
import Members from 'containers/Members/Members'
import JoinTeam from 'containers/JoinTeam/JoinTeam'
import Projects from 'containers/Projects/Projects'

// Hoc
import Layout from 'hoc/Layout/Layout'
import DashboardLayout from 'hoc/DashboardLayout/DashboardLayout'

import './App.css'

class App extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    isVerified: PropTypes.bool.isRequired,
    checkAuth: PropTypes.bool.isRequired,
    userUID: PropTypes.string,
    teamName: PropTypes.string,
    checkTeam: PropTypes.bool.isRequired,
  }

  state = {
    hideNavigation: false,
  }

  componentDidMount() {
    const {props} = this

    const rootRoute = props.location.pathname.split('/')[1]

    props.getCurrentUser()

    if (rootRoute === 'join-team') {
      this.setState({hideNavigation: true})
    } else {
      this.setState({hideNavigation: false})
    }
  }

  componentDidUpdate(prevProps) {
    const {props} = this

    // If the user changes or checkingAuth had finished we update the currentTeam.
    if (prevProps.checkAuth !== props.checkAuth || prevProps.userUID !== props.userUID) {
      props.getCurrentTeam(props.userUID)
    }
  }

  render() {
    const {props, state} = this

    // User is unauthenticated
    let route = (
      <Layout hideNavigation={state.hideNavigation}>
        <Switch>
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />

          <Route path="/join-team" component={JoinTeam} />
          <Route
            render={() => (
              <Redirect
                to={{
                  pathname: '/login',
                  state: {from: props.location},
                }}
              />
            )}
          />
        </Switch>
      </Layout>
    )

    // User is authenticated
    if (props.isAuthenticated) {
      const {from} = props.location.state || {from: {pathname: '/home'}}

      route =
        props.teamName && !props.checkTeam !== null ? (
          <DashboardLayout>
            <Switch>
              <Route path="/home" exact render={() => <p> your team is {props.teamName}</p>} />
              <Route path="/dashboard" exact render={() => <h1>Dashboard</h1>} />
              <Route path="/members" exact component={Members} />
              <Route path="/updates" exact render={() => <p>Updates Page</p>} />
              <Route path="/projects" exact component={Projects} />
              <Route render={() => <Redirect to={from} />} />
            </Switch>
          </DashboardLayout>
        ) : (
          <Switch>
            <Route path="/create-team" component={CreateTeam} />
            <Route
              render={() => (
                <Redirect to={{pathname: '/create-team', state: {from: props.location}}} />
              )}
            />
          </Switch>
        )
    }

    // User is not verified
    if (props.isAuthenticated && !props.isVerified) {
      route = (
        <Switch>
          <Route path="/home" render={() => <h1>You're not verified</h1>} />
          <Route render={() => <Redirect to="/home" />} />
        </Switch>
      )
    }

    // Render Loading screen while checking auth and team.
    return !props.checkAuth && !props.checkTeam ? route : <p>Loading</p>
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.currentUser.uid !== null, // true if user isAuthenticated
  isVerified: state.auth.currentUser.emailVerified, //  true is email is emailVerified
  checkAuth: state.auth.checkAuth, // loading for checkingAuth
  userUID: state.auth.currentUser.uid, // user id
  checkTeam: state.team.checkTeam, // loading for checkingTeam
  teamName: state.team.name, // team name
})

const mapDispatchToProps = dispatch => ({
  getCurrentUser: () => dispatch(getCurrentUserAction()),
  getCurrentTeam: userUID => dispatch(getCurrentTeamAction(userUID)),
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
)
