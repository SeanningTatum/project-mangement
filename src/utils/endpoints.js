const PROJECT_ID = 'project-management-84ed9'
const REGION = 'asia-northeast1'

export const registerUserAPI = `https://${REGION}-${PROJECT_ID}.cloudfunctions.net/createUser`
