import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'

const apolloClient = new ApolloClient({
  uri: 'http://localhost:8080/graphql',
})

export default apolloClient

// Queries
export const FETCH_USER_QUERY = gql`
  query User($userID: ID!) {
    User(id: $userID) {
      team {
        id
        name
      }
    }
  }
`

export const FETCH_TEAM_MEMBERS_QUERY = gql`
  query Team($teamID: ID!) {
    Team(id: $teamID) {
      members {
        displayName
        email
      }
    }
  }
`
export const JOIN_TEAM_QUERY = gql`
  query Team($teamID: ID!) {
    Team(id: $teamID) {
      name
      invite_key
    }
  }
`

// Mutations
export const CREATE_TEAM_MUTATION = gql`
  mutation createTeam($teamName: String!, $userUID: ID!) {
    createTeam(teamName: $teamName, userUID: $userUID) {
      name
      id
    }
  }
`

export const INVITE_MEMBERS_MUTATION = gql`
  mutation inviteMembers($teamID: ID!, $emails: [String], $sentBy: String!, $senderEmail: String!) {
    inviteMembers(teamID: $teamID, emails: $emails, sentBy: $sentBy, senderEmail: $senderEmail)
  }
`

export const JOIN_TEAM_MUTATION = gql`
  mutation joinTeam(
    $teamID: ID!
    $userUID: ID!
    $displayName: String!
    $email: String!
    $inviteKey: String!
  ) {
    joinTeam(
      teamID: $teamID
      userUID: $userUID
      displayName: $displayName
      email: $email
      inviteKey: $inviteKey
    )
  }
`

export const ADD_PROJECT_MUTATION = gql`
  mutation addProject(
    $team_id: ID!
    $name: String
    $description: String
    $contactPerson: String
    $time: String
    $day: String
    $emailReceivers: [String]
    $emailSender: String
    $emailSenderName: String
    $emailFrequency: String
  ) {
    addProject(
      team_id: $team_id
      name: $name
      description: $description
      contactPerson: $contactPerson
      time: $time
      day: $day
      emailReceivers: $emailReceivers
      emailSender: $emailSender
      emailSenderName: $emailSenderName
      emailFrequency: $emailFrequency
    )
  }
`
