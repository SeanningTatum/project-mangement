import * as firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyBQnCvtYlgtHop7cQVkOvSFgRBW-1zd7Qo',
  authDomain: 'project-management-84ed9.firebaseapp.com',
  databaseURL: 'https://project-management-84ed9.firebaseio.com',
  projectId: 'project-management-84ed9',
  storageBucket: 'project-management-84ed9.appspot.com',
  messagingSenderId: '553710531797',
}

firebase.initializeApp(config)

export default firebase
