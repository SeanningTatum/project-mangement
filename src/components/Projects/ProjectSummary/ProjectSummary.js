import React from 'react'
import {Card, CardText, Col, Row} from 'reactstrap'

import './ProjectSummary.css'

const projectSummary = () => (
  <Card body className="project-summary--container">
    <div className="project-summary--header">
      <p className="mb-0">Activity</p>
    </div>

    <div className="project-summary--body">
      <Row className="text-center">
        <Col md={4}>
          <CardText>Number of Projects</CardText>
        </Col>
        <Col md={4}>
          <CardText>Active Projects</CardText>
        </Col>
        <Col md={4}>
          <CardText>% Finished</CardText>
        </Col>
      </Row>
    </div>
  </Card>
)

export default projectSummary
