import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Mutation} from 'react-apollo'
import {FormGroup, Form} from 'reactstrap'
import {Input, Modal} from 'components/UI/index'
import {validateEmail} from 'utils/helperFunctions'
import {ADD_PROJECT_MUTATION} from 'utils/apollo'

class addProjectModal extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
  }

  state = {
    projectName: '',
    projectDescription: '',
    contactPerson: '',
    emailRecievers: '',
    emailSender: '',
    emailSenderName: '', // Project Manager?
    emailFrequency: '',
    time: new Date(),
    day: new Date().getUTCDay(),
  }

  onSubmitHandler = (event, addProjectMutation) => {
    event.preventDefault()
    const tempForm = {...this.state}

    // Transform the strings with commas to an array
    const emailRecieversArr = tempForm.emailRecievers.split(',').map(email => {
      if (validateEmail(email.trim()) && email.trim() !== '') {
        return email.trim()
      }
    })

    tempForm.emailRecievers = emailRecieversArr
    console.log(tempForm)

    addProjectMutation({variables: tempForm})
  }

  onInputChangedHandler = (event, formName) => this.setState({[formName]: event.target.value})

  render() {
    const {props, state} = this
    const {open, toggle} = props

    return (
      <Modal isOpen={open} toggle={toggle} action="Add Project" title="Add Project">
        <Mutation mutation={ADD_PROJECT_MUTATION}>
          {(addProject, {data}) => (
            <Form onSubmit={e => this.onSubmitHandler(e, addProject)}>
              {/* Project Name */}
              <FormGroup>
                <label htmlFor="projectName">Project Name</label>
                <Input
                  value={state.projectName}
                  name="projectName"
                  onChange={e => this.onInputChangedHandler(e, 'projectName')}
                />
              </FormGroup>

              {/* Project Description */}
              <FormGroup>
                <label htmlFor="projectDescription">Project Description</label>
                <Input
                  value={state.projectDescription}
                  name="projectDescription"
                  onChange={e => this.onInputChangedHandler(e, 'projectDescription')}
                />
              </FormGroup>

              {/* Contact Person */}
              <FormGroup>
                <label htmlFor="contactPerson">Contact Person</label>
                <Input
                  value={state.contactPerson}
                  name="contactPerson"
                  onChange={e => this.onInputChangedHandler(e, 'contactPerson')}
                />
              </FormGroup>

              {/* Email Recievers */}
              <FormGroup>
                <label htmlFor="emailRecievers">Email Recievers</label>
                <Input
                  value={state.emailRecievers}
                  name="emailRecievers"
                  onChange={e => this.onInputChangedHandler(e, 'emailRecievers')}
                />
              </FormGroup>

              {/* Email Sender */}
              <FormGroup>
                <label htmlFor="emailSender">Email Sender</label>
                <Input
                  value={state.emailSender}
                  name="emailSender"
                  onChange={e => this.onInputChangedHandler(e, 'emailSender')}
                />
              </FormGroup>

              {/* Email Sender Name */}
              <FormGroup>
                <label htmlFor="emailSenderName">Email Sender Name</label>
                <Input
                  value={state.emailSenderName}
                  name="emailSender"
                  onChange={e => this.onInputChangedHandler(e, 'emailSenderName')}
                />
              </FormGroup>

              {/* Email Frequency */}
              <FormGroup>
                <label htmlFor="emailFrequency">Email Frequency</label>
                <Input
                  value={state.emailFrequency}
                  name="emailFrequency"
                  onChange={e => this.onInputChangedHandler(e, 'emailFrequency')}
                />
              </FormGroup>

              {/* Time */}
              <FormGroup>
                <label htmlFor="time">Time</label>
                <Input
                  value={state.time}
                  name="time"
                  onChange={e => this.onInputChangedHandler(e, 'time')}
                />
              </FormGroup>

              {/* Day */}
              <FormGroup>
                <label htmlFor="day">Day</label>
                <Input
                  value={state.day}
                  name="day"
                  onChange={e => this.onInputChangedHandler(e, 'day')}
                />
              </FormGroup>
            </Form>
          )}
        </Mutation>
      </Modal>
    )
  }
}

export default addProjectModal
