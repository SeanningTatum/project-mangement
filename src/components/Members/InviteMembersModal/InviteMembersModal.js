import React from 'react'
import PropTypes from 'prop-types'
import Modal from 'components/UI/Modal/Modal'
import Input from 'components/UI/Input/Input'
import {FormGroup} from 'reactstrap'
import {Mutation} from 'react-apollo'
import {INVITE_MEMBERS_MUTATION} from 'utils/apollo'

const inviteMembersModal = props => {
  const {
    modalOpen,
    toggleModal,
    onSubmitHandler,
    inviteLength,
    addInviteLength,
    formChangeHandler,
    form,
  } = props

  return (
    <Mutation mutation={INVITE_MEMBERS_MUTATION}>
      {(inviteMembers, {data}) => (
        <Modal
          isOpen={modalOpen}
          toggle={toggleModal}
          title="Invite Members"
          action="Invite"
          onSubmit={() => onSubmitHandler(inviteMembers)}
        >
          {inviteLength.map(number => (
            <FormGroup key={number}>
              <label htmlFor={`email${number}`}>Member #{number}</label>
              <Input
                type="email"
                name={`email${number}`}
                onChange={e => formChangeHandler(`email${number}`, e)}
                value={form[`email${number}`]}
              />
            </FormGroup>
          ))}

          <div className="text-right">
            <p style={{cursor: 'pointer'}} onClick={addInviteLength}>
              Invite More +
            </p>
          </div>
        </Modal>
      )}
    </Mutation>
  )
}

inviteMembersModal.propTypes = {
  modalOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  onSubmitHandler: PropTypes.func.isRequired,
  inviteLength: PropTypes.arrayOf(PropTypes.number).isRequired,
  addInviteLength: PropTypes.func.isRequired,
  formChangeHandler: PropTypes.func.isRequired,
  form: PropTypes.object.isRequired,
}

export default inviteMembersModal
