import React from 'react'
import PropTypes from 'prop-types'
import {Card, Button, CardTitle, CardText, Row, Col} from 'reactstrap'

const memberList = ({members}) => (
  <Row>
    {members.map(member => (
      <Col md={3} key={member.email}>
        <Card body className="text-center">
          <CardTitle>{member.displayName}</CardTitle>
          <CardText>{member.email}</CardText>
          <Button>Go somewhere</Button>
        </Card>
      </Col>
    ))}
  </Row>
)

memberList.propTypes = {
  members: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string,
      email: PropTypes.string,
    })
  ),
}

export default memberList
