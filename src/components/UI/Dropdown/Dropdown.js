import React from 'react'
import PropTypes from 'prop-types'
import {UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap'

const Dropdown = ({name, onLogout}) => (
  <UncontrolledDropdown>
    <DropdownToggle caret>{name}</DropdownToggle>
    <DropdownMenu>
      <DropdownItem onClick={onLogout}>Logout</DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
)

Dropdown.propTypes = {
  name: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired,
}

export default Dropdown
