import React from 'react'
import {configure, shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Input from './Input'

configure({adapter: new Adapter()})

describe('<Input />', () => {
  it('value should be the same as prop.value', () => {
    const wrapper = shallow(<Input value="test" onChange={() => null} />)

    expect(wrapper.props().value).toEqual('test')
  })

  it("value should equal '' if not given in props", () => {
    const wrapper = shallow(<Input onChange={() => null} />)

    const input = wrapper.find('input')

    expect(input.props().value).toEqual('')
  })

  it("type should equal to 'text' if not given in props", () => {
    const wrapper = shallow(<Input onChange={() => null} />)

    const input = wrapper.find('input')

    expect(input.props().type).toEqual('text')
  })

  it("placeholder should equal to '' if not given in props", () => {
    const wrapper = shallow(<Input onChange={() => null} />)

    const input = wrapper.find('input')

    expect(input.props().placeholder).toEqual('')
  })

  it('should trigger onChange function when onChange event happens', () => {
    const onChangeMock = jest.fn()
    const wrapper = shallow(<Input onChange={onChangeMock} />)

    wrapper.find('input').simulate('change', {target: {value: 'new value'}})
    expect(onChangeMock).toBeCalled()
  })
})
