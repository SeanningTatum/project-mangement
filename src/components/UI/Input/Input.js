import React from 'react'
import PropTypes from 'prop-types'

const Input = props => {
  const {type, placeholder, value, onChange} = props

  return (
    <input
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      className="form-control"
    />
  )
}

Input.propTypes = {
  placeholder: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
}

Input.defaultProps = {
  placeholder: '',
  value: '',
  type: 'text',
}

export default Input
