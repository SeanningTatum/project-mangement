/* eslint-disable react/button-has-type */
import React from 'react'
import PropTypes from 'prop-types'

const Button = ({children, color, onClick, type}) => (
  <button className={`btn btn-${color}`} onClick={onClick} type={type}>
    {children}
  </button>
)

Button.propTypes = {
  children: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string,
}

Button.defaultProps = {
  type: 'button',
}

export default Button
