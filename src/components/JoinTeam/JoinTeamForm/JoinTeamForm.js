import React from 'react'
import PropTypes from 'prop-types'
import {Mutation} from 'react-apollo'
import {Form, FormGroup} from 'reactstrap'
import Input from 'components/UI/Input/Input'
import Button from 'components/UI/Button/Button'
import {JOIN_TEAM_MUTATION} from 'utils/apollo'

const joinTeamForm = props => {
  const {
    inputChangedHandler,
    confirmPassword,
    password,
    nickname,
    onSubmitHandler,
    onErrorHandler,
  } = props

  return (
    <Mutation mutation={JOIN_TEAM_MUTATION} onError={() => onErrorHandler()}>
      {joinTeam => (
        <Form onSubmit={event => onSubmitHandler(event, joinTeam)}>
          {/* Nickname */}
          <FormGroup>
            <label htmlFor="nickname">Nickname</label>
            <Input
              onChange={event => inputChangedHandler(event, 'nickname')}
              name="nickname"
              value={nickname}
            />
          </FormGroup>
          {/* Password */}
          <FormGroup>
            <label htmlFor="password">Password</label>
            <Input
              onChange={event => inputChangedHandler(event, 'password')}
              name="password"
              value={password}
            />
          </FormGroup>
          {/* Confirm Password */}
          <FormGroup>
            <label htmlFor="confirmPassword">Confirm Password</label>
            <Input
              onChange={event => inputChangedHandler(event, 'confirmPassword')}
              name="confirmPassword"
              value={confirmPassword}
            />
          </FormGroup>
          <div className="text-right">
            <Button color="primary" type="submit">
              Join Team
            </Button>
          </div>
        </Form>
      )}
    </Mutation>
  )
}

joinTeamForm.propTypes = {
  inputChangedHandler: PropTypes.func.isRequired,
  onErrorHandler: PropTypes.func.isRequired,
  onSubmitHandler: PropTypes.func.isRequired,
  confirmPassword: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  nickname: PropTypes.string.isRequired,
}

export default joinTeamForm
