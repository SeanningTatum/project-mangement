import React from 'react'
import {Collapse, Navbar, NavbarToggler, NavbarBrand, Nav} from 'reactstrap'

import NavigationItems from '../NavigationItems/NavigationItems'

class NavigationBar extends React.Component {
  state = {
    isOpen: false,
  }

  toggle() {
    this.setState(prevState => ({isOpen: !prevState.isOpen}))
  }

  render() {
    const {state} = this

    return (
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={state.isOpen} navbar>
          <Nav navbar>
            <NavigationItems />
          </Nav>
        </Collapse>
      </Navbar>
    )
  }
}

export default NavigationBar
