import React from 'react'
import PropTypes from 'prop-types'
import {NavItem} from 'reactstrap'
import {NavLink} from 'react-router-dom'

const NavigationItem = ({name, to}) => (
  <NavItem>
    <NavLink to={to} className="nav-link">
      {name}
    </NavLink>
  </NavItem>
)

NavigationItem.propTypes = {
  name: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
}

export default NavigationItem
