import React, {Fragment} from 'react'

import NavigationItem from './NavigationItem/NavigationItem'

const NavigationItems = () => (
  <Fragment>
    <NavigationItem to="/register" name="Register" />
    <NavigationItem to="/login" name="Login" />
  </Fragment>
)

export default NavigationItems
