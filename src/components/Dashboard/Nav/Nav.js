import React from 'react'
import Dropdown from 'components/UI/Dropdown/Dropdown'
import './Nav.css'

const DashboardNav = ({currentUser, onLogout}) => (
  <nav className="navbar navbar-light">
    <div className="navbar-brand--container">
      <a className="navbar-brand" href="/">
        Super OS
      </a>
    </div>

    <div className="block-count--container">
      <p className="mb-0">
        Blocks Submitted: <strong>0 / 2</strong>
      </p>
    </div>

    <div className="notif--container">
      <i className="material-icons">notifications_none</i>
    </div>

    <div className="current-user--container">
      <Dropdown name={currentUser} onLogout={onLogout} />
    </div>
  </nav>
)

export default DashboardNav
