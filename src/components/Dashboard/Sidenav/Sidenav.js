import React from 'react'

import SidenavItem from './SidenavItem/SidenavItem'
import './Sidenav.css'

const Sidenav = () => (
  <div className="dashboard__sidenav">
    <SidenavItem to="/home" iconName="home" />
    <SidenavItem to="/members" iconName="group" />
    <SidenavItem to="/projects" iconName="group_work" />
    <SidenavItem to="/updates" iconName="mail" />
  </div>
)

export default Sidenav
