import React from 'react'
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom'

const Icon = ({to, iconName}) => (
  <NavLink to={to} exact className="nav-link">
    <i className="material-icons">{iconName}</i>
  </NavLink>
)

Icon.propTypes = {
  to: PropTypes.string.isRequired,
  iconName: PropTypes.string.isRequired,
}

export default Icon
