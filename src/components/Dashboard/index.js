import Nav from './Nav/Nav'
import SideNav from './Sidenav/Sidenav'

export const DashboardSideNav = SideNav
export const DashboardNav = Nav
