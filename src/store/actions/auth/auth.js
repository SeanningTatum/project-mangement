import {validateEmail} from 'utils/helperFunctions'
import firebase from 'utils/firebase'
import axios from 'axios'
import {registerUserAPI} from 'utils/endpoints'

import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
  SAVE_CURRENT_USER,
  LOGOUT,
} from './actionTypes'

// [LOGOUT START]
const logout = () => ({type: LOGOUT})

export const onLogout = () => async dispatch => {
  console.log('onLogout')

  try {
    await firebase.auth().signOut()

    localStorage.clear('team_name')
    localStorage.clear('team_id')

    dispatch(logout())
  } catch (error) {
    console.error(error)
  }
}
// [LOGOUT END]

// [GET_CURRENT_USER START]
const saveCurrentUser = payload => ({
  type: SAVE_CURRENT_USER,
  payload,
})

export const getCurrentUser = () => dispatch => {
  firebase.auth().onAuthStateChanged(user => {
    if (user) {
      dispatch(saveCurrentUser(user))
    } else {
      dispatch(saveCurrentUser())
    }
  })
}
// [GET_CURRENT_USER END]

// [LOGIN START]
const loginSuccess = ({uid, email, displayName, emailVerified}) => ({
  type: LOGIN_SUCCESS,
  payload: {
    uid,
    email,
    displayName,
    emailVerified,
  },
})

const loginError = payload => ({
  type: LOGIN_ERROR,
  payload,
})

export const onLogin = ({email, password}) => async dispatch => {
  try {
    const {user} = await firebase.auth().signInWithEmailAndPassword(email, password)
    dispatch(loginSuccess(user))
  } catch (error) {
    console.error(error)
    dispatch(loginError(error))
  }
}
// [LOGIN END]

// [REGISTER START]
const registerError = errorMessage => ({
  type: REGISTER_ERROR,
  payload: {
    errorMessage,
  },
})

const registerSuccess = ({uid, email, displayName, emailVerified}) => ({
  type: REGISTER_SUCCESS,
  payload: {
    uid,
    email,
    displayName,
    emailVerified,
  },
})

export const onRegister = form => async dispatch => {
  const {email, password, fullName, confirmPassword} = form

  try {
    // Check if passwords and email are valid
    if (password !== confirmPassword) throw new Error('Passwords do not match')
    if (!validateEmail(email)) throw new Error('Email is not valid!')

    // Create User and retrieve user data
    const {user} = await firebase.auth().createUserWithEmailAndPassword(email, password)
    user.updateProfile({displayName: fullName})

    const userObj = {
      displayName: fullName,
      uid: user.uid,
      email,
    }

    // Create User in datastore too
    await axios.post(registerUserAPI, userObj)

    user.sendEmailVerification()

    dispatch(registerSuccess(user))
  } catch (error) {
    console.error(error.message)
    dispatch(registerError(error.message))

    // Delete the user in firebase if error occurs in cloud function
    const user = await firebase.auth().currentUser
    console.log('user', user)
    if (user) user.delete()
  }
}
// [REGISTER END]
