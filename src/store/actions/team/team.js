import apolloClient, {FETCH_USER_QUERY} from 'utils/apollo'

import {SAVE_CURRENT_TEAM, GET_CURRENT_TEAM_START} from './types'

const getCurrentTeamStart = () => ({
  type: GET_CURRENT_TEAM_START,
})

export const saveCurrentTeam = ({id, name}) => ({
  type: SAVE_CURRENT_TEAM,
  payload: {id, name},
})

export const getCurrentTeam = userUID => async dispatch => {
  dispatch(getCurrentTeamStart())

  // If there's no userUID -> User is not logged in
  if (!userUID) {
    dispatch(saveCurrentTeam({id: null, name: null}))
    return
  }

  // Get team_name and team_id from localStorage / cache
  // This is persistent if user never logs out
  const teamName = localStorage.getItem('team_name')
  const teamID = localStorage.getItem('team_id')

  // If there was an existing team -> Save team to redux
  // These data remains here until the user explicitly logs out.
  if (teamName && teamID) {
    dispatch(saveCurrentTeam({name: teamName, id: teamID}))
  }

  // Fetch the user's team to see if the team_name is still the same
  // because the name might be updated.
  console.log('[Fetching users team...]')
  const {data} = await apolloClient.query({
    query: FETCH_USER_QUERY,
    variables: {userID: userUID},
  })

  // If the user has a team
  if (data.User.team) {
    const {name, id} = data.User.team

    // update the team name if it has been updated
    if (teamName !== name || id !== teamID) {
      localStorage.setItem('team_name', name)
      localStorage.setItem('team_id', id)
      dispatch(saveCurrentTeam({id, name}))
    }
  } else {
    // If there's no team -> set id and name to null

    localStorage.removeItem('team_name')
    localStorage.removeItem('team_id')

    dispatch(saveCurrentTeam({id: null, name: null}))
  }
}
