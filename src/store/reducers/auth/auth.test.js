import * as actionTypes from 'store/actions/auth/actionTypes'

import reducer from './auth'

describe('authReducer', () => {
  let initialState

  beforeEach(() => {
    initialState = {
      currentUser: {
        emailVerified: false,
        uid: null,
        displayName: null,
        email: null,
      },
      errorMessage: '',
      checkAuth: true,
    }
  })

  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  // LOGIN_SUCCESS
  it(`should store user info upon ${actionTypes.LOGIN_SUCCESS}`, () => {
    const loginSuccessAction = {
      type: actionTypes.LOGIN_SUCCESS,
      payload: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
        email: 'test@test.com',
      },
    }

    expect(reducer(initialState, loginSuccessAction)).toEqual({
      ...initialState,
      currentUser: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
        email: 'test@test.com',
      },
      checkAuth: false,
    })
  })

  // SAVE_CURRENT_USER
  it(`should save user info on ${actionTypes.SAVE_CURRENT_USER}`, () => {
    const saveCurrentUserAction = {
      type: actionTypes.SAVE_CURRENT_USER,
      payload: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
      },
    }

    expect(reducer(initialState, saveCurrentUserAction)).toEqual({
      ...initialState,
      currentUser: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
      },
      checkAuth: false,
    })
  })

  // LOGOUT
  it(`should clear all user info upon ${actionTypes.LOGOUT}`, () => {
    const logoutAction = {
      type: actionTypes.LOG_OUT,
    }

    expect(reducer(initialState, logoutAction)).toEqual(initialState)
  })

  // REGISTER_SUCCESS
  it(`should store user info after ${actionTypes.REGISTER_SUCCESS}`, () => {
    const registerSuccessAction = {
      type: actionTypes.REGISTER_SUCCESS,
      payload: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
      },
      checkAuth: false,
    }

    expect(reducer(initialState, registerSuccessAction)).toEqual({
      ...initialState,
      currentUser: {
        uid: 'test-uid',
        emailVerified: true,
        displayName: 'test-displayName',
      },
      checkAuth: false,
    })
  })

  // REGISTER ERROR
  it(`should have an errorMessage on ${actionTypes.REGISTER_ERROR}`, () => {
    const registerErrorAction = {
      type: actionTypes.REGISTER_ERROR,
      payload: {
        errorMessage: 'Could not register your account',
      },
    }

    expect(reducer(initialState, registerErrorAction)).toEqual({
      ...initialState,
      errorMessage: 'Could not register your account',
      checkAuth: false,
    })
  })

  it(`should have an errorMessage on ${actionTypes.LOGIN_ERROR}`, () => {
    const registerErrorAction = {
      type: actionTypes.LOGIN_ERROR,
      payload: {
        errorMessage: 'Invalid Username or Password',
      },
    }

    expect(reducer(initialState, registerErrorAction)).toEqual({
      ...initialState,
      errorMessage: 'Invalid Username or Password',
      checkAuth: false,
    })
  })
})
