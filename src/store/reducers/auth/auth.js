import {
  REGISTER_SUCCESS,
  LOGIN_SUCCESS,
  REGISTER_ERROR,
  LOGIN_ERROR,
  SAVE_CURRENT_USER,
  LOGOUT,
} from 'store/actions/auth/actionTypes'

// [Start Initial State]
const initialState = {
  currentUser: {
    emailVerified: false,
    uid: null,
    displayName: null,
    email: null,
  },
  errorMessage: '',
  checkAuth: true,
}
// [End Initial State]

// [Start Functions]
const saveCurrentUser = (state, action) => {
  // If there is no user
  if (!action.payload) {
    return {
      ...state,
      currentUser: {
        emailVerified: false,
        uid: null,
        displayName: null,
      },
      checkAuth: false,
    }
  }

  const {uid, emailVerified, displayName, email} = action.payload

  return {
    ...state,
    currentUser: {uid, emailVerified, displayName, email},
    checkAuth: false,
  }
}

const logout = state => ({
  ...state,
  currentUser: {
    emailVerified: false,
    uid: null,
    displayName: null,
  },
})

const authError = (state, action) => {
  const {errorMessage} = action.payload

  return {
    ...state,
    errorMessage,
    currentUser: {
      emailVerified: false,
      uid: null,
      displayName: null,
      email: null,
    },
    checkAuth: false,
  }
}
// [End Functions]

// [Start Reducer]
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return saveCurrentUser(state, action)

    case SAVE_CURRENT_USER:
      return saveCurrentUser(state, action)

    case LOGIN_SUCCESS:
      return saveCurrentUser(state, action)

    case LOGOUT:
      return logout(state)

    case REGISTER_ERROR:
      return authError(state, action)

    case LOGIN_ERROR:
      return authError(state, action)

    default:
      return state
  }
}

// [End Functions]

export default reducer
