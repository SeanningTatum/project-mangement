import {SAVE_CURRENT_TEAM, GET_CURRENT_TEAM_START} from 'store/actions/team/types'

const initialState = {
  id: null,
  name: null,
  checkTeam: false,
}

const saveCurrentTeam = (state, action) => {
  const {id, name} = action.payload

  return {...state, id, name, checkTeam: false}
}

const getCurrentTeamStart = state => ({...state, checkTeam: true})

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_CURRENT_TEAM:
      return saveCurrentTeam(state, action)

    case GET_CURRENT_TEAM_START:
      return getCurrentTeamStart(state)

    default:
      return state
  }
}

export default reducer
